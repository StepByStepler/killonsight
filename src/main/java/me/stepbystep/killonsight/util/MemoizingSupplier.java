package me.stepbystep.killonsight.util;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class MemoizingSupplier<T> implements Supplier<T> {
    private final Supplier<T> valueProvider;
    private final long cacheMillis;
    private T cachedValue;
    private long expireMillis = 0;

    public MemoizingSupplier(Supplier<T> valueProvider, int cacheTime, TimeUnit unit) {
        cacheMillis = unit.toMillis(cacheTime);
        this.valueProvider = valueProvider;
    }

    @Override
    public T get() {
        long expireMillis = this.expireMillis;
        long now = System.currentTimeMillis();
        if (now >= expireMillis) {
            synchronized (this) {
                cachedValue = valueProvider.get();
                this.expireMillis = now + cacheMillis;
            }
        }

        return cachedValue;
    }
}
