package me.stepbystep.killonsight;

import masecla.mlib.main.MLib;
import me.stepbystep.killonsight.commands.KillOnSightCommand;
import me.stepbystep.killonsight.containers.SelfTargetsContainer;
import me.stepbystep.killonsight.containers.TopTargetsContainer;
import me.stepbystep.killonsight.managers.KillOnSightManager;
import org.bukkit.plugin.java.JavaPlugin;

public class KillOnSightPlugin extends JavaPlugin {
    @Override
    public void onEnable() {
        MLib lib = new MLib(this);
        lib.getConfigurationAPI().requireAll();
        KillOnSightManager manager = new KillOnSightManager(lib);

        new KillOnSightCommand(lib, manager).register();
        new SelfTargetsContainer(lib, manager).register();
        new TopTargetsContainer(lib, manager).register();
    }
}
