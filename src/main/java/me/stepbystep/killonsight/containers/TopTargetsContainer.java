package me.stepbystep.killonsight.containers;

import masecla.mlib.classes.Replaceable;
import masecla.mlib.classes.builders.ItemBuilder;
import masecla.mlib.containers.generic.ImmutableContainer;
import masecla.mlib.main.MLib;
import me.stepbystep.killonsight.managers.KillOnSightManager;
import me.stepbystep.killonsight.models.TopTargetData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class TopTargetsContainer extends ImmutableContainer {
    private final KillOnSightManager targetsManager;

    public TopTargetsContainer(MLib lib, KillOnSightManager targetsManager) {
        super(lib);
        this.targetsManager = targetsManager;
    }

    @Override
    public int getSize() {
        return 54;
    }

    @Override
    public int getUpdatingInterval() {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean requiresUpdating() {
        return false;
    }

    @Override
    public Inventory getInventory(Player player) {
        String title = lib.getMessagesAPI().getPluginMessage("top-targets");
        Inventory inv = Bukkit.createInventory(null, getSize(), title);
        List<TopTargetData> topTargets = targetsManager.findTopTargets();

        for (int i = 0; i < topTargets.size(); i++) {
            TopTargetData targetData = topTargets.get(i);
            OfflinePlayer targetPlayer = Bukkit.getOfflinePlayer(targetData.getPlayerUUID());
            Replaceable targetInfo = new Replaceable("%count%", String.valueOf(targetData.getTargetedCount()));
            String targetedCountMsg = lib.getMessagesAPI().getPluginMessage("targeted-count", targetInfo);

            ItemStack icon = new ItemBuilder()
                    .skull(targetPlayer)
                    .name(ChatColor.GREEN + targetPlayer.getName())
                    .lore(targetedCountMsg)
                    .build(lib);
            inv.setItem(i, icon);
        }

        return inv;
    }

    @Override
    public void onTopClick(InventoryClickEvent event) {
        event.setCancelled(true);
    }

    public void onDrop(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }
}
