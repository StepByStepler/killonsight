package me.stepbystep.killonsight.containers;

import masecla.mlib.classes.builders.ItemBuilder;
import masecla.mlib.containers.instances.SquaredPagedContainer;
import masecla.mlib.main.MLib;
import me.stepbystep.killonsight.managers.KillOnSightManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.stream.Collectors;

public class SelfTargetsContainer extends SquaredPagedContainer {
    private final KillOnSightManager targetsManager;

    public SelfTargetsContainer(MLib lib, KillOnSightManager targetsManager) {
        super(lib);
        this.targetsManager = targetsManager;
    }

    @Override
    public List<ItemStack> getOrderableItems(Player player) {
        return targetsManager
                .findTargets(player.getUniqueId())
                .stream()
                .map(targetUUID -> {
                    OfflinePlayer targetPlayer = Bukkit.getOfflinePlayer(targetUUID);
                    return new ItemBuilder()
                            .name(ChatColor.GREEN + targetPlayer.getName())
                            .skull(targetPlayer)
                            .build(lib);
                })
                .collect(Collectors.toList());
    }

    @Override
    public void usableClick(InventoryClickEvent inventoryClickEvent) {
        // nothing
    }

    @Override
    public int getSize() {
        return 54;
    }

    @Override
    public int getUpdatingInterval() {
        return Integer.MAX_VALUE;
    }
}
