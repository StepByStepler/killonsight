package me.stepbystep.killonsight.models;

import me.stepbystep.killonsight.managers.KillOnSightManager;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class TopTargetsSupplier implements Supplier<List<TopTargetData>> {
    private static final long CACHE_MILLIS = TimeUnit.MINUTES.toMillis(1);

    private final KillOnSightManager manager;
    private List<TopTargetData> cachedValue;
    private long expireMillis = 0;

    public TopTargetsSupplier(KillOnSightManager manager) {
        this.manager = manager;
    }

    @Override
    public List<TopTargetData> get() {
        long expireMillis = this.expireMillis;
        long now = System.currentTimeMillis();
        if (now >= expireMillis) {
            synchronized (this) {
                cachedValue = manager.loadTopTargets();
                this.expireMillis = now + CACHE_MILLIS;
            }
        }

        return cachedValue;
    }
}
