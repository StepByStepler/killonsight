package me.stepbystep.killonsight.models;

import java.util.UUID;

public class TopTargetData {
    private final UUID playerUUID;
    private final int targetedCount;

    public TopTargetData(UUID playerUUID, int targetedCount) {
        this.playerUUID = playerUUID;
        this.targetedCount = targetedCount;
    }

    public UUID getPlayerUUID() {
        return playerUUID;
    }

    public int getTargetedCount() {
        return targetedCount;
    }
}
