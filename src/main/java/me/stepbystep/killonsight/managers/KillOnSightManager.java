package me.stepbystep.killonsight.managers;

import masecla.mlib.classes.MamlConfiguration;
import masecla.mlib.main.MLib;
import me.stepbystep.killonsight.models.TopTargetData;
import me.stepbystep.killonsight.models.TopTargetsSupplier;
import org.bukkit.configuration.ConfigurationSection;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.*;
import java.util.stream.Collectors;

@ParametersAreNonnullByDefault
public class KillOnSightManager {
    private final MamlConfiguration dbConfig;
    private final TopTargetsSupplier topTargetsSupplier = new TopTargetsSupplier(this);

    public KillOnSightManager(MLib lib) {
        dbConfig = lib.getConfigurationAPI().getConfig("database");
    }

    public List<TopTargetData> loadTopTargets() {
        Map<UUID, Integer> targetedPlayers = new HashMap<>();
        for (String playerKey : dbConfig.getKeys(false)) {
            ConfigurationSection playerSection = dbConfig.getConfigurationSection(playerKey);
            for (String targetKey : playerSection.getKeys(false)) {
                UUID targetUUID = UUID.fromString(targetKey);
                int newCount = targetedPlayers.computeIfAbsent(targetUUID, uuid -> 0) + 1;
                targetedPlayers.put(targetUUID, newCount);
            }
        }

        return targetedPlayers.entrySet()
                .stream()
                .sorted(Comparator.<Map.Entry<UUID, Integer>>comparingInt(Map.Entry::getValue).reversed())
                .limit(45)
                .map(e -> new TopTargetData(e.getKey(), e.getValue()))
                .collect(Collectors.toList());
    }

    public List<UUID> findTargets(UUID ownerUUID) {
        ConfigurationSection userSection = dbConfig.getConfigurationSection(ownerUUID.toString());
        if (userSection == null) return Collections.emptyList();

        return userSection.getKeys(false)
                .stream()
                .map(UUID::fromString)
                .collect(Collectors.toList());
    }

    public List<TopTargetData> findTopTargets() {
        return topTargetsSupplier.get();
    }

    public boolean addTarget(UUID ownerUUID, UUID targetUUID) {
        ConfigurationSection userSection = getOrCreateSection(ownerUUID.toString());
        String targetKey = targetUUID.toString();
        if (userSection.getBoolean(targetKey)) {
            return false;
        } else {
            userSection.set(targetUUID.toString(), true);
            dbConfig.markAsNeedingSave();
            return true;
        }
    }

    private ConfigurationSection getOrCreateSection(String sectionName) {
        ConfigurationSection existingSection = dbConfig.getConfigurationSection(sectionName);
        return existingSection == null ? dbConfig.createSection(sectionName) : existingSection;
    }
}
