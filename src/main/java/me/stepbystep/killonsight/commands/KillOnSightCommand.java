package me.stepbystep.killonsight.commands;

import masecla.mlib.annotations.RegisterableInfo;
import masecla.mlib.annotations.SubcommandInfo;
import masecla.mlib.classes.Registerable;
import masecla.mlib.main.MLib;
import me.stepbystep.killonsight.containers.SelfTargetsContainer;
import me.stepbystep.killonsight.containers.TopTargetsContainer;
import me.stepbystep.killonsight.managers.KillOnSightManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

@RegisterableInfo(command = "kos")
public class KillOnSightCommand extends Registerable {
    private final KillOnSightManager targetsManager;

    public KillOnSightCommand(MLib lib, KillOnSightManager targetsManager) {
        super(lib);
        this.targetsManager = targetsManager;
    }

    @SubcommandInfo(permission = "", subcommand = "add")
    public void addTargetWithoutArgs(Player player) {
        lib.getMessagesAPI().sendMessage("wrong-input", player);
    }

    @SubcommandInfo(permission = "", subcommand = "add", async = true)
    public void addTarget(Player player, String[] args) {
        //noinspection deprecation
        OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
        if (!(target.isOnline() || target.hasPlayedBefore())) {
            lib.getMessagesAPI().sendMessage("wrong-player", player);
            return;
        }

        UUID targetUUID = target.getUniqueId();
        if (targetUUID.equals(player.getUniqueId())) {
            lib.getMessagesAPI().sendMessage("self-target", player);
            return;
        }

        if (targetsManager.addTarget(player.getUniqueId(), targetUUID)) {
            lib.getMessagesAPI().sendMessage("added-successfully", player);
        } else {
            lib.getMessagesAPI().sendMessage("already-targeted", player);
        }
    }

    @SubcommandInfo(permission = "", subcommand = "top", async = true)
    public void showTopPlayers(Player player) {
        lib.getContainerAPI().openFor(player, TopTargetsContainer.class);
    }

    @SubcommandInfo(permission = "", subcommand = "list", async = true)
    public void showMyPlayers(Player player) {
        lib.getContainerAPI().openFor(player, SelfTargetsContainer.class);
    }
}
